﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace base64
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static string Base64Encode(string plainText)
        {
            try
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
                return System.Convert.ToBase64String(plainTextBytes);
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
                return "";
            }
           
        }
        public static string Base64Decode(string base64EncodedData)
        {
            try
            {
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch (Exception e )
            {

                MessageBox.Show(e.Message);
                return "";
            }
          
        }
        private void btnDec_Click(object sender, EventArgs e)
        {
            txtSonuc.Text = Base64Decode(txtVeri.Text.Trim());

        }

        private void btnEnc_Click(object sender, EventArgs e)
        {
            txtSonuc.Text = Base64Encode(txtVeri.Text.Trim());
        }
    }
}
